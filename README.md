# Assignment 7: Feature Matching

## Synopsis

In this homework assignment, we put into practice the code we have seen in lecture for feature detection and matching. This assignment has three parts. The first part of the assignment requires completing the feature matching code, the second part requires capturing five different images, and the third part requires performing and optimizing feature detection and matching.

Detailed [instructions](https://docs.google.com/document/d/1v15hpI9rzkRWW0qVbUdDUKcQqdthvRFHPry2hap_dWA/edit?usp=sharing) and the [report template](https://docs.google.com/presentation/d/1tQ-1-bBW9eLKjwvzKLnAznhA4LU1amRvyHJfbhftIHc/edit?usp=sharing) are available on Google drive.